package Tampilan_GUI_UKM;

import Tampilan_GUI_UKM.UKM;
import Tampilan_GUI_UKM.Penduduk;
import Tampilan_GUI_UKM.Masyarakat;
import Tampilan_GUI_UKM.Mahasiswa;

public class newMain {
public static void main(String[] args) {
        double tot = 0;
        double totIMahasiswa= 0, totIMasyarakat = 0;
        UKM aktif = new UKM("Kereukunan Keluarga Paingan");

        Mahasiswa ketua = new Mahasiswa();
        ketua.setNama("Audy");
        ketua.setNim("195314142");
        ketua.setTTl("Makassar, 14 Februari 2001");
        aktif.setKetua(ketua);
        
        Mahasiswa sekretaris = new Mahasiswa();
        sekretaris.setNama("Alexander");
        sekretaris.setNim("195314127");
        sekretaris.setTTl("Jakarta, 11 Januari 2001");
        aktif.setSekretaris(sekretaris);

        Penduduk[] P = new Penduduk[4];
        Mahasiswa[] anggotaS = new Mahasiswa[2];
        anggotaS[0] = new Mahasiswa();
        anggotaS[0].setNama("Piter");
        anggotaS[0].setNim("195314135");
        anggotaS[0].setTTl("Balikpapan, 24 Mei 2001");
        P[0] = anggotaS[0];
        anggotaS[1] = new Mahasiswa();
        anggotaS[1].setNama("Jeane");
        anggotaS[1].setNim("195314111");
        anggotaS[1].setTTl("Surabaya, 17 juni 2001");
        P[1] = anggotaS[1];
        
        Masyarakat[] anggotaM = new Masyarakat[2];
        anggotaM[0] = new Masyarakat();
        anggotaM[0].setNama("Suyatno");
        anggotaM[0].setNomor("108");
        anggotaM[0].setTTl("Jember, 27 Apri 1988");
        P[2] = anggotaM[0];
        anggotaM[1] = new Masyarakat();
        anggotaM[1].setNama("Joko");
        anggotaM[1].setNomor("107");
        anggotaM[1].setTTl("Madura, 12 juli 1992");
        P[3] = anggotaM[1];
    
        System.out.println("=================================================================================================");
        System.out.println("=-==-==-==-==-==-==-==-==-==-==-==-==-==  Ketua  UKM  ==-==-==-==-==-==-==-==-==-==-==-==-==-==-=");
        System.out.println("=================================================================================================");
        System.out.println("Nama                : "+ketua.getNama());
        System.out.println("NIM                 : "+ketua.getNim());
        System.out.println("Tempat Tanggal Lahir: "+ketua.getTTL());
        System.out.println("-------------------------------------------------------------------------------------------------"); 
        System.out.println("");
        System.out.println("=================================================================================================");
        System.out.println("=-==-==-==-==-==-==-==-==-==-==-==-==-  Sekretaris UKM  -==-==-==-==-==-==-==-==-==-==-==-==-==-=");
        System.out.println("=================================================================================================");
        System.out.println("Nama                : "+sekretaris.getNama());
        System.out.println("NIM                 : "+sekretaris.getNim());
        System.out.println("Tempat Tanggal Lahir: "+sekretaris.getTTL());
        System.out.println("-------------------------------------------------------------------------------------------------"); 
        System.out.println("");
        System.out.println("=================================================================================================");
        System.out.println("=-==-==-==-==-==-==-==-==Daftar Anggota dan Iurannya anggota UKM PAINGAN=-==-==-==-==-==-==-==-==");
        System.out.println("=================================================================================================");
        System.out.println("Anggota\tNama\tTempat Tanggal Lahir\tNim/Nomor\tIuran");
        System.out.println("-------------------------------------------------------------------------------------------------");   
        for (int i = 0; i < P.length; i++) {
            if(P[i] instanceof Mahasiswa){
                Mahasiswa M = (Mahasiswa) P[i];
                System.out.println((i + 1)+"\t"+P[i].getNama()+"\t"+P[i].getTTL()+"\t"+M.getNim()+"\t"+M.hitungiuran());
                totIMahasiswa =+ M.hitungiuran();
            }else if(P[i] instanceof Masyarakat){
                Masyarakat S = (Masyarakat) P[i];
                System.out.println((i + 1)+"\t"+P[i].getNama()+"\t"+P[i].getTTL()+"\t"+S.getNomor()+"\t\t"+S.hitungiuran());
                totIMasyarakat =+ S.hitungiuran();
            }
        }
        System.out.println("-------------------------------------------------------------------------------------------------");
        tot = totIMahasiswa + totIMasyarakat;
        System.out.println("Total Uang Iuran Anggota : "+tot);
        System.out.println("-------------------------------------------------------------------------------------------------"); 
    }
}